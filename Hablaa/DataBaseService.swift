//
//  DataBaseService.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import Foundation
import RealmSwift

class DataBaseService {
    
    //MARK: Language
    func saveLanguage(language: Language) {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(language)
        }
    }
    
    func getLanguages() -> Array<Language> {
        let realm = try! Realm()
        let languages = realm.objects(Language.self)
        return Array(languages)
    }
    
    func deleteLanguage(language: Language) {
        
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(language)
        }
        
    }
    
}
