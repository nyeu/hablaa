//
//  AppDelegate.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        IQKeyboardManager.sharedManager().enable = true

        
        let initialVC = TranslationViewController()
        let navigationController = UINavigationController(rootViewController: initialVC)
        navigationController.navigationBar.isTranslucent = false
        
        window?.rootViewController = navigationController
        
        return true
    }

  

}

