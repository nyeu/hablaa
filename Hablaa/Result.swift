//
//  Result.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class Result: Object, Mappable {
    
    dynamic var text: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "text"
    }
    
    func mapping(map: Map) {
        
        text <- map["text"]        
    }
    
}
