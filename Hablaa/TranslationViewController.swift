//
//  TranslationViewController.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import UIKit

class TranslationViewController: UIViewController {

    //MARK: Outlets
    
    @IBOutlet weak var wordToTranslateTextField: UITextField!
    @IBOutlet weak var translateButton: UIButton!
    @IBOutlet weak var resultsTableView: UITableView!
    

    let model = TranslationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       model.selectedLanguages = DataBaseService().getLanguages()
    }

    func setupTableView() {
        resultsTableView.dataSource = self
        resultsTableView.delegate = self
    }
    
    func runTimer() {
        model.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(TranslationViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func updateTimer() {
        model.seconds -= 1     //This will decrement(count down)the seconds.
        
        if (model.seconds == 0) {
            let alert = UIAlertController(title: "Error", message: "Your query has took more than 10 seconds to load :/", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
            
            
            alert.addAction(UIAlertAction(title: "Reload", style: .default, handler: { (action) in
                self.loadTranslation()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func translateTapped(_ sender: Any) {
        loadTranslation()
    }
    
    func loadTranslation() {
        if let word = wordToTranslateTextField.text, let selectedLanguages = model.selectedLanguages {
            
            for language in selectedLanguages {
                let network = NetworkService()
                network.translate(word: word, from: "eng", to: language.lang_code!) { (response) in
                    print(response)
                    
                    switch(response.result) {
                        
                    case .success(_):
                        
                        if let jsonResponse = response.result.value as? Array<[String: Any]>{
                            self.model.translations?.append((MappingService().mapResult(json: jsonResponse)?.first)!)
                            
                            if (self.model.translations?.count == self.model.selectedLanguages?.count) {
                                self.resultsTableView.reloadData()
                            }
                        }
                        
                    case .failure(_):
                        print(response.result.error!);
                    }
                    
                }
            }
        }
   
    }

    @IBAction func settingsTapped(_ sender: Any) {
        let settingsVC = SettingsViewController()
        navigationController?.pushViewController(settingsVC, animated: true)
    }
}

extension TranslationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let translations = model.translations {
            return translations.count
        }
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
    
        if (cell == nil) {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        }
        
        if let translation = model.translations?[indexPath.row] {
            
            cell?.textLabel?.text = translation.text
           //cell?.detailTextLabel?.text =
        }
       
        
        return cell!
    }
}

