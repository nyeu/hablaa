//
//  SettingsViewModel.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import Foundation

protocol SettingsDelegate {
    func showLanguages()
}

class SettingsViewModel {
    
    var languages: Array<Language>?
    var selectedLanguages: Array<Language>? = DataBaseService().getLanguages()
    
    var delegate: SettingsDelegate?
    
    func getLanguages() {
        
        let networkService = NetworkService()
        networkService.getLanguages { (response) in
            
            switch(response.result) {
                
            case .success(_):
                
                if let jsonResponse = response.result.value as? Array<[String: Any]>{
                    self.languages = MappingService().mapLanguage(json: jsonResponse)
                    self.delegate?.showLanguages()
                }
                
            case .failure(_):
                print(response.result.error!);
            }
            
        }
    }
}
