//
//  MapperService.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import Foundation
import ObjectMapper

struct MappingService {
    
    public func mapLanguage(json: Array<[String : Any]>) -> Array<Language>? {
        return Mapper<Language>().mapArray(JSONArray: json)
    }
    
    public func mapResult(json: Array<[String : Any]>) -> Array<Result>? {
        return Mapper<Result>().mapArray(JSONArray: json)
    }
}
