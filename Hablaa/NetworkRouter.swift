//
//  NetworkRouter.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//


import Foundation
import Alamofire

enum NetworkRouter: URLRequestConvertible{
    
    static let baseURLString = "http://hablaa.com/hs/"
    
    case translate(word: String, from: String, to: String),
    getLanguages()
    
    var method: Alamofire.HTTPMethod {
        switch self {
            
        case .translate(_),
             .getLanguages():
            return .get
            
        }
    }
    
    var path: String {
        
        switch self {
            
        case .translate(_):
            return "translation/"
            
        case .getLanguages():
            return "languages/"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: NetworkRouter.baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
            
        case .getLanguages():
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: nil)
            
        case .translate(let word, let from, let to):
            let newURL = URL(string: "\(word)/\(from)-\(to)", relativeTo: urlRequest.url!);
            urlRequest.url = newURL;

            return try Alamofire.URLEncoding.default.encode(urlRequest, with: nil)
            
            
        default:
            return urlRequest
        }
    }
    
}
