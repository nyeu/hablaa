//
//  TranslationViewModel.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import Foundation

protocol TranslationDelegate {
    func showResults()
}

class TranslationViewModel {
    
    var delegate: TranslationDelegate?
    var selectedLanguages: Array<Language>? = DataBaseService().getLanguages()
    var translations: Array<Result>? = Array()
    
    var timer = Timer()
    var seconds = 10
}
