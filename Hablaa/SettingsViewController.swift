//
//  SettingsViewController.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    //MARK: Variables
    let model = SettingsViewModel()
    
    //MARK: Outlets
    @IBOutlet weak var languagesTableView: UITableView!

    //MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self
        model.getLanguages()
        setupTableView()
    }
 
    func setupTableView() {
        languagesTableView.dataSource = self
        languagesTableView.delegate = self
    }
}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if let languages = model.languages {
            return languages.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        
        if let language: Language = model.languages?[indexPath.row] {
            cell.textLabel?.text = language.name
            
            cell.accessoryType = .none
            
            let contains = model.selectedLanguages!.contains { $0.lang_code == language.lang_code }
            
            if (contains) {
                cell.accessoryType = .checkmark
            }

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let language: Language = model.languages?[indexPath.row] {
            let contains = model.selectedLanguages!.contains { $0.lang_code == language.lang_code }

            if (contains){
                
                if let index = model.selectedLanguages?.index(of: language) {
                    model.selectedLanguages?.remove(at: index)
                }
                
                DataBaseService().deleteLanguage(language: language)
            }
            else{
                model.selectedLanguages?.append(language)
                DataBaseService().saveLanguage(language: language)
            }
        }
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
    }
}

extension SettingsViewController: SettingsDelegate {
    
    func showLanguages() {
        languagesTableView.reloadData()
    }
}


