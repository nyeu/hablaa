//
//  NetworkService.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import Foundation
import Alamofire

class NetworkService {
    
    func getLanguages( completionHandler: @escaping (DataResponse<Any>) -> Void) {
     
        Alamofire.request(NetworkRouter.getLanguages()).responseJSON {
            (response) in
            completionHandler(response)
        }
    }
    
    func translate(word: String, from: String, to: String, completionHandler: @escaping (DataResponse<Any>) -> Void) {
        
        Alamofire.request(NetworkRouter.translate(word: word, from: from, to: to)).responseJSON { (response) in
            completionHandler(response)
        }
    }
    
}
