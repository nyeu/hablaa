//
//  Language.swift
//  Hablaa
//
//  Created by Joan on 07/06/2017.
//  Copyright © 2017 JCS. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class Language: Object, Mappable {
    
    dynamic var name: String?
    dynamic var lang_code: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "lang_code"
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        lang_code <- map["lang_code"]
        
    }
    
}
